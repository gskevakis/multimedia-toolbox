package com.skevakis.video;

import static org.junit.Assert.*;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.skevakis.multimedia.VideoManipulator;
import com.skevakis.multimedia.model.VideoInfo;

@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration
public class VideoManipulationTest {

    private static final Logger log = LoggerFactory.getLogger(VideoManipulationTest.class);

    @Autowired
    VideoManipulator videoManipulator = null;
    
    @Test
    public void testThumbnail() {
	File input = new File(getClass().getResource("/video.m4v").getFile());
	File output = null;
	VideoInfo info = null;
	
	try {
	    Path p = Files.createTempFile("output", ".jpg");
	    output = p.toFile();

	    info = videoManipulator.getScreenshot(input, output);
	} catch (Exception e) {
	    log.info("exception", e);
	    e.printStackTrace();
	}
	
	log.info("Output size {}", output.length());
	assertTrue(output.length() > 0);
	
	assertEquals(28, info.getDuration());
	assertEquals(202, info.getWidth());
	assertEquals(360, info.getHeight());
	
	output.delete();
    }
}
