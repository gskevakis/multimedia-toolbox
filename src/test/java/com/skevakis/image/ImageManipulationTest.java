package com.skevakis.image;

import static org.junit.Assert.*;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.skevakis.multimedia.ImageManipulator;
import com.skevakis.multimedia.model.ImageInfo;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class ImageManipulationTest {

    private static final Logger log = LoggerFactory.getLogger(ImageManipulationTest.class);

    @Test
    public void testThumbnail() {
	File input = new File(getClass().getResource("/image.jpg").getFile());
	File output = null;
	ImageInfo info = null;
	
	try {
	    Path p = Files.createTempFile("output", ".jpg");
	    output = p.toFile();

	    info = ImageManipulator.createThumbnail(input, output, 500, 500);
	} catch (Exception e) {
	    log.info("", e);
	    e.printStackTrace();
	}

	log.info("Output size {}", output.length());
	assertTrue(output.length() > 0);
	assertEquals(1200, info.getHeight());
	assertEquals(1920, info.getWidth());

	output.delete();
    }
}
