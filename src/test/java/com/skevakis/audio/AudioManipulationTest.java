package com.skevakis.audio;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.skevakis.multimedia.AudioManipulator;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class AudioManipulationTest {

    private static final Logger log = LoggerFactory.getLogger(AudioManipulationTest.class);

    @Test
    public void audioDuration() {
	File input = new File(getClass().getResource("/audio.mp3").getFile());
	Long l = null;

	try {
	    l = AudioManipulator.getDuration(input);
	} catch (Exception e) {
	    log.info("", e);
	}

	assertEquals(73, l.longValue());
    }
    
    @Test
    public void invalidAudioFile() {
	File input = new File(getClass().getResource("/image.jpg").getFile());
	Long l = null;

	try {
	    l = AudioManipulator.getDuration(input);
	} catch (Exception e) {
	    log.info("", e);
	}

	assertTrue(l == 0);
    }
}
