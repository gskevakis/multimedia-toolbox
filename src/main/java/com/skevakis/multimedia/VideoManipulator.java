package com.skevakis.multimedia;

import com.skevakis.multimedia.model.VideoInfo;
import com.xuggle.mediatool.IMediaReader;
import com.xuggle.mediatool.MediaListenerAdapter;
import com.xuggle.mediatool.ToolFactory;
import com.xuggle.mediatool.event.IVideoPictureEvent;
import com.xuggle.xuggler.Global;
import com.xuggle.xuggler.ICodec;
import com.xuggle.xuggler.IStream;
import com.xuggle.xuggler.IStreamCoder;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.imageio.ImageIO;
import org.apache.tika.Tika;
import org.apache.tika.mime.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VideoManipulator {

    private static final Logger log = LoggerFactory.getLogger(VideoManipulator.class);

    private static long defaultImageTimestamp = 0;

    // The video stream index, used to ensure we display frames from one and
    // only one video stream from the media container.
    private int mVideoStreamIndex = -1;

    // Instance variables
    // ////////////////////////////////

    private BufferedImage image;
    private long imageTimestamp = 0;
    private ImageSnapListener listener;
    IMediaReader mediaReader;
    VideoInfo info = new VideoInfo();

    /**
     * Get a screenshot of a video. The middle frame is used if possible.
     * Returns metadata about the video.
     *
     * @param in
     *            The input video.
     * @param out
     *            The output image.
     *
     */
    public VideoInfo getScreenshot(File in, File out) {
	Tika tika = new Tika();
	String mime = tika.detect(out.getName());
	MediaType mediaType = MediaType.parse(mime);

	try {
	    return this.getScreenshot(in, new FileOutputStream(out), mediaType.getSubtype());
	} catch (FileNotFoundException e) {
	    log.error("File not found", e);
	}
	return null;
    }

    public VideoInfo getScreenshot(File in, OutputStream out, String outputImageFormat) {
	mediaReader = ToolFactory.makeReader(in.getAbsolutePath());
	mediaReader.setBufferedImageTypeToGenerate(BufferedImage.TYPE_3BYTE_BGR);

        listener = new ImageSnapListener();
	mediaReader.addListener(listener);

	mediaReader.open();

	log.debug("Video duration {}", mediaReader.getContainer().getDuration());
	imageTimestamp = mediaReader.getContainer().getDuration() / 2;
	log.debug("Image will be retrived at {}", imageTimestamp);
	info.setDuration(mediaReader.getContainer().getDuration() / Global.DEFAULT_PTS_PER_SECOND);

	int numSteams = mediaReader.getContainer().getNumStreams();
	for (int i = 0; i < numSteams; i++) {
	    IStream stream = mediaReader.getContainer().getStream(i);
	    IStreamCoder coder = stream.getStreamCoder();
	    if (coder.getCodecType() == ICodec.Type.CODEC_TYPE_VIDEO) {
		info.setHeight(coder.getHeight());
		info.setWidth(coder.getWidth());
                break;
	    }
	}

	// read out the contents of the media file and dispatch events to the
	// attached listener
	while (mediaReader.readPacket() == null)
	    ;

	try {
	    ImageIO.write(image, outputImageFormat, out);
	} catch (IOException e) {
	    log.error("", e);
	}
	return info;
    }

    private class ImageSnapListener extends MediaListenerAdapter {

	@Override
	public void onVideoPicture(IVideoPictureEvent event) {
	    if (event.getStreamIndex() != mVideoStreamIndex) {
		// if the selected video stream id is not yet set, go ahead and
		// select this lucky video stream
		if (mVideoStreamIndex == -1) {
		    mVideoStreamIndex = event.getStreamIndex();
		} else {
		    // no need to show frames from this video stream
		    return;
		}
            }
//            log.trace("Video timestamp at {}", event.getTimeStamp());

            if (event.getTimeStamp() == defaultImageTimestamp) {
                log.debug("Image retrieved at {}", event.getTimeStamp());
		image = event.getImage();
	    }

	    if (event.getTimeStamp() >= imageTimestamp) {
		log.debug("Image retrieved at {}", event.getTimeStamp());
		image = event.getImage();
		mediaReader.removeListener(listener);
	    }
	}
    }
}
