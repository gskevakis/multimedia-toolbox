package com.skevakis.multimedia;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.imageio.ImageIO;

import org.apache.tika.Tika;
import org.apache.tika.mime.MediaType;

import com.skevakis.multimedia.model.ImageInfo;

public class ImageManipulator {

    /**
     * Create a thumbnail of an image, without exceeding a specific window limit
     * specified in pixels.
     * 
     * @param in
     *            The input image file.
     * @param out
     *            The output image file.
     * @param maxWidth
     *            The max width in pixels.
     * @param maxHeight
     *            The max height in pixels.
     * @throws IOException
     */
    public static ImageInfo createThumbnail(File in, File out, int maxWidth, int maxHeight) throws IOException {
	return createThumbnail(new FileInputStream(in), out, maxWidth, maxHeight);
    }

    /**
     * Create a thumbnail of an image, without exceeding a specific window limit
     * specified in pixels.
     * 
     * @param in
     *            The input stream.
     * @param out
     *            The output image file.
     * @param maxWidth
     *            The max width in pixels.
     * @param maxHeight
     *            The max height in pixels.
     * @throws IOException
     */
    public static ImageInfo createThumbnail(InputStream in, File out, int maxWidth, int maxHeight) throws IOException {
	// detect the format of the output file by checking its extension
	Tika tika = new Tika();
	String mime = tika.detect(out.getName());
	MediaType mediaType = MediaType.parse(mime);

	String format = mediaType.getSubtype();

	return createThumbnail(in, new FileOutputStream(out), maxWidth, maxHeight, format);
    }

    /**
     * Create a thumbnail of an image, without exceeding a specific window limit
     * specified in pixels.
     * 
     * @param in
     *            The input stream.
     * @param out
     *            The output stream.
     * @param maxWidth
     *            The max width in pixels.
     * @param maxHeight
     *            The max height in pixels.
     * @param outputFormat
     *            The format of the thumbnail image.
     * @throws IOException
     */
    public static ImageInfo createThumbnail(InputStream in, OutputStream out, int maxWidth, int maxHeight, String outputFormat) throws IOException {
	ImageInfo info = new ImageInfo();
	Image img = ImageIO.read(in);

	info.setHeight(img.getHeight(null));
	info.setWidth(img.getWidth(null));
	
	if (img.getWidth(null) > maxWidth) {
	    // transform the input image without exceeding the width
	    // limit
	    img = img.getScaledInstance(maxWidth, -1, Image.SCALE_SMOOTH);
	}

	if (img.getHeight(null) > maxHeight) {
	    // transform the input image without exceeding max width
	    img = img.getScaledInstance(-1, maxHeight, Image.SCALE_SMOOTH);
	}

	// Create a buffered image with transparency
	BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_RGB);

	// Draw the image on to the buffered image
	Graphics2D bGr = bimage.createGraphics();
	bGr.drawImage(img, 0, 0, null);
	bGr.dispose();

	ImageIO.write(bimage, outputFormat, out);
	return info;
    }
}
