package com.skevakis.multimedia;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xuggle.mediatool.IMediaReader;
import com.xuggle.mediatool.ToolFactory;
import com.xuggle.xuggler.Global;

public class AudioManipulator {

    private static final Logger log = LoggerFactory.getLogger(AudioManipulator.class);

    /**
     * Get the duration of an audio file.
     * 
     * @param in
     *            The input image stream.
     * @return The duration in seconds or 0 if the file is not supported.
     * @throws IOException
     */
    public static Long getDuration(File in) throws IOException {

	IMediaReader mediaReader = ToolFactory.makeReader(in.getAbsolutePath());
	mediaReader.open();

	long duration = mediaReader.getContainer().getDuration() / Global.DEFAULT_PTS_PER_SECOND;
	mediaReader.close();
	log.debug("Audio duration (in seconds): {}", duration);
	return duration;
    }
}
